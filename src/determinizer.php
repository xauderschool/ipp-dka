<?php

// DKA:xsvana01

require_once "automata.php";

//-----
// Vykonava determinizaciu konecneho automatu
//-----

class Determinizer {
	private $originalAutomata;
	private $newAutomata;

	private $notProcessedStates = array();

	public function __construct($originalAutomata) {
		$this->originalAutomata = $originalAutomata;
	}

	public function run() {
		$this->originalAutomata->removeEpsilonRules();
		$this->newAutomata = new FiniteAutomata();
		$this->determinize();

		return $this->newAutomata;
	}

	private function determinize() {
		$startState = $this->originalAutomata->getStartState();
		$this->notProcessedStates[] = array($startState);

		while(count($this->notProcessedStates))
			foreach($this->notProcessedStates as $notProcessedState)
				$this->processState($notProcessedState);

		$this->newAutomata->setStartState($startState);

		foreach($this->originalAutomata->getSymbols() as $symbol)
			$this->newAutomata->addSymbol($symbol);
	}

	private function processState($state) {
		$this->notProcessedStates = $this->removeFromNotProcessed($state);
		$this->newAutomata->addState($this->serializeState($state));
		$symbols = $this->originalAutomata->getSymbols();
		$finalStates = $this->originalAutomata->getFinalStates();

		foreach($symbols as $symbol)
			$this->processStateWithSymbol($state, $symbol);

		if(count(array_intersect($finalStates, $state)))
			$this->newAutomata->addFinalState($this->serializeState($state));
	}

	private function processStateWithSymbol($state, $symbol) {
		$accessibleStates = $this->getAccessibleStates($state, $symbol);
		$ruleState = $this->serializeState($state);
		$ruleNewState = $this->serializeState($accessibleStates);

		if(count($accessibleStates))
			$this->newAutomata->addRule($ruleState, $symbol, $ruleNewState);

		$states = $this->newAutomata->getStates();
		$states[] = "";

		if(in_array($ruleNewState, $states) == FALSE)
			$this->notProcessedStates[] = $accessibleStates;
	}

	private function getAccessibleStates($state, $symbol) {
		$accessibleStates = array();
		$rules = $this->originalAutomata->getRules();

		foreach($state as $s)
			foreach($rules as $rule)
				if($rule["state"] == $s && $rule["symbol"] == $symbol)
				if(in_array($rule["newState"], $accessibleStates) == FALSE)
					$accessibleStates[] = $rule["newState"];

		return $accessibleStates;
	}

	private function serializeState($state) {
		$output = "";
		sort($state);

		$innerStatesCount = count($state);
		$i = 1;

		foreach($state as $s) {
			$output .= $s;
			if($i != $innerStatesCount) $output .= "_";
			$i++;
		}

		return $output;
	}

	private function removeFromNotProcessed($state) {
		$newNotProcessed = array();

		foreach($this->notProcessedStates as $notProcessedState)
			if(count(array_diff($notProcessedState, $state)) != 0)
				$newNotProcessed[] = $notProcessedState;

		return $newNotProcessed;
	}
}
