<?php

// DKA:xsvana01

//-----
// Skript na determinizaciu konecneho automatu
//-----

require_once "parameters.php";
require_once "help.php";
require_once "scanner.php";
require_once "parser.php";
require_once "determinizer.php";

//-----
// Zistenie parametrov
//-----

try {
	$parameters = new Parameters();
	$parameters->getAndCheck();
} 
catch(ParametersError $e) {
	error_log($e->getMessage());
	exit(1);
}

//-----
// Pripadne vypisanie napovedy
//-----

if($parameters->getHelp()) {
	showHelp();
	exit(0);
}

//-----
// Ziskanie vstupneho retazca
//-----

if($inputFile = $parameters->getInput()) {
	$inputString = file_get_contents($inputFile);

	if($inputString === FALSE) {
		error_log("Could not open input file");
		exit(2);
	}
}

else {
	$inputString = file_get_contents("php://stdin");
}

//-----
// Skenovanie a parsovanie
//-----

$scanner = new Scanner($inputString);
$parser = new Parser($scanner, $parameters->getCaseInsensitive());

try {
	$finiteAutomata = $parser->createFiniteAutomata();
}
catch(ScannerError $e) {
	error_log($e->getMessage());
	exit(40);
}
catch(ParseError $e) {
	error_log($e->getMessage());
	exit(40);
}

//-----
// Overenie spravnosti automatu
//-----

try {
	$finiteAutomata->validate();
}

catch(FiniteAutomataError $e) {
	error_log($e->getMessage());
	error_log($inputFile);
	exit(41);
}

//-----
// Odstranenie epsilon pravidiel
//-----

if($parameters->getRemoveEpsilonRules()) {
	$finiteAutomata->removeEpsilonRules();
	$output = $finiteAutomata->getAsString();
}

//-----
// Determinizacia
//-----

else if($parameters->getDeterminize()) {
	$determinizer = new Determinizer($finiteAutomata);
	$dfa = $determinizer->run();
	$output = $dfa->getAsString();
}

//-----
// Neurobime ziadnu akciu, len sformatujeme
//-----

else {
	$output = $finiteAutomata->getAsString();
}

//-----
// Ulozenie/zobrazenie vysledku
//-----

if($outputFile = $parameters->getOutput()) {
	if(file_put_contents($outputFile, $output) === FALSE) {
		error_log("Could not write to file");
		exit(4);
	}
}
