<?php

//-----
// Vynimka vyvolana v pripade, ze konecny automat 
// nie je spravne definovany
//-----

class FiniteAutomataError extends Exception {};

//-----
// Reprezentacia konecneho automatu
//-----

class FiniteAutomata {
	private $states = array();
	private $symbols = array();
	private $rules = array();
	private $startState;
	private $finalStates = array();

	private $hasEpsilonRules = FALSE;

	public function getAsString() {
		sort($this->states);

		sort($this->symbols);
		sort($this->finalStates);
		usort($this->rules, "ruleSort");

		$returnString = "(\n{"; 
		
		for($i=0; $i<count($this->states); $i++) {
			$returnString .= $this->states[$i];
			if($i != count($this->states) - 1)
				$returnString .= ", ";
		}

		$returnString .= "},\n{";

		for($i=0; $i<count($this->symbols); $i++) {
			$returnString .= "'" . $this->symbols[$i] . "'";
			if($i != count($this->symbols) - 1)
				$returnString .= ", ";
		}

		$returnString .="},\n{\n";

		for($i=0; $i<count($this->rules); $i++) {

			$rule = $this->rules[$i];
			$returnString .= $rule["state"]." '".$rule["symbol"]."' -> ".$rule["newState"];

			if($i != count($this->rules) - 1)
				$returnString .= ",";

			$returnString .= "\n";
		}

		$returnString .= "},\n$this->startState,\n{";

		for($i=0; $i<count($this->finalStates); $i++) {
			$returnString .= $this->finalStates[$i];
			if($i != count($this->finalStates) - 1)
				$returnString .= ", ";
		}

		$returnString .= "}\n)";

		return $returnString;
	}

	public function validate() {
		if(count($this->symbols) == 0)
			throw new FiniteAutomataError("No input symbols set");

		if(!in_array($this->startState, $this->states))
			throw new FiniteAutomataError("Start state not in states set");

		if(array_diff($this->finalStates, $this->states) == $this->states)
			throw new FiniteAutomataError("Some final state not in states set");

		foreach($this->rules as $rule) {
			if(!in_array($rule["state"], $this->states))
				throw new FiniteAutomataError("Start state of rule not in states set");

			if(!in_array($rule["symbol"], $this->symbols) && $rule["symbol"] != "")
				throw new FiniteAutomataError("Symbol of rule not in symbols set");

			if(!in_array($rule["newState"], $this->states))
				throw new FiniteAutomataError("New state of rule not in states set");
		}
	}

	//-----
	// Gettery
	//-----

	public function getStates() { return $this->states; }
	public function getSymbols() { return $this->symbols; }
	public function getRules() { return $this->rules; }
	public function getStartState() { return $this->startState; }
	public function getFinalStates() { return $this->finalStates; }

	//-----
	// Nasledujuce metody naplnaju automat stavmi, symbolmi, pravidlami
	//-----

	public function addState($state) {
		if(in_array($state, $this->states))
			return FALSE;

		$this->states[] = $state;
	}

	public function addSymbol($symbol) {
		if(in_array($symbol, $this->symbols))
			return FALSE;

		$this->symbols[] = $symbol;
	}

	public function addRule($state, $symbol, $newState) {
		$rule = array(
			"state" => $state,
			"symbol" => $symbol,
			"newState" => $newState);

		foreach($this->rules as $testRule)
			if($testRule == $rule) return FALSE;

		$this->rules[] = $rule;

		if($symbol == "")
			$this->hasEpsilonRules = TRUE;
	}

	public function setStartState($state) {
		$this->startState = $state;
	}

	public function addFinalState($state) {
		$this->finalStates[] = $state;
	}

	//-----
	// Odstranenie epsilon pravidiel nevytvara novy automat, ale
	// modifikuje existujuci
	//-----

	public function removeEpsilonRules() {
		if($this->hasEpsilonRules == FALSE)
			return;

		$newRules = array();
		$newFinalStates = array();

		foreach($this->states as $p) {
			foreach($this->epsilonClosure($p) as $r) 
				foreach($this->states as $q)
					foreach($this->symbols as $a)
						if($this->ruleExists($r, $a, $q))
							$newRules[] = array(
								"state" => $p,
								"symbol" => $a,
								"newState" => $q);
		}
		
		foreach($this->states as $q) {
			$closureAndFinalIntersect = array_intersect(
				$this->finalStates, $this->epsilonClosure($q));

			if(count($closureAndFinalIntersect) != 0)
				$newFinalStates[] = $q;
		}

		$this->rules = $newRules;
		$this->finalStates = $newFinalStates;
		$this->hasEpsilonRules = FALSE;
	}

	public function determinize() {
		$this->removeEpsilonRules();

	}

	private function epsilonClosure($state) {
		if(!in_array($state, $this->states))
			throw new FiniteAutomataError("State $state does not exist in this FA");

		$i = 0;
		$epsilonClosure[0] = array($state);

		do {
			$i++;
			$incClosure = $this->epsilonClosureIncrease($epsilonClosure[$i-1]);

			$epsilonClosure[$i] = array_merge($epsilonClosure[$i-1], $incClosure);
			$epsilonClosure[$i] = array_unique($epsilonClosure[$i]);
		} 
		while($epsilonClosure[$i] != $epsilonClosure[$i-1]);

		return $epsilonClosure[$i];
	}

	private function epsilonClosureIncrease($epsilonClosure) {
		$newClosure = array();

		foreach($this->states as $p)
			foreach($epsilonClosure as $q) {
				if($this->ruleExists($q, "", $p))
					$newClosure[] = $p;
			}

		return $newClosure;
	}

	private function ruleExists($state, $symbol, $newState) {
		foreach ($this->rules as $rule)
			if($rule["state"] == $state &&
				 $rule["symbol"] == $symbol &&
				 $rule["newState"] == $newState)
				return TRUE;

		return FALSE;
	}
}

//-----
// Funkcia na zoradenie pravidiel
//-----

function ruleSort($a, $b) {
	$res = strcmp($a["state"], $b["state"]);
	if($res != 0) return $res;

	$res = strcmp($a["symbol"], $b["symbol"]);
	if($res != 0) return $res;

	$res = strcmp($a["newState"], $b["newState"]);
	if($res != 0) return $res;
}
