<?php

// DKA:xsvana01;

//-----
// Vynimka vyvolana, ak sa zo zostavajuceho vstupu nepodarilo 
// vytvorit token
//-----

class ScannerError extends Exception {};

//-----
// Scanner vykona lexikalnu analyzu vstupneho retazca
//-----

class Scanner {
	private static $ignoreRegexs = array(
		"COMMENT" => "/^#.*[\n\r]/",
		"END_COMMENT" => "/^#.*$/",
		"WHITESPACE" => "/^\s/");

	private static $tokenRegexs = array(
		"PARENTHESIS_LEFT" => "/^\(/",
		"PARENTHESIS_RIGHT" => "/^\)/",
		"BRACKET_LEFT" => "/^\{/",
		"BRACKET_RIGHT" => "/^\}/",
		"STATE" => "/^[a-zA-Z][a-zA-Z0-9_]*/",
		"SYMBOL" => "/^'([^']|'')*'/",
		"RULE" => "/^\-\>/",
		"COMMA" => "/^,/",
		"EOF" => "/^$/");

	private $currentToken;
	private $inputString;
	private $tokens = array();

	public function __construct($inputString) {
		$this->inputString = $inputString;
	}

	public function getNext() {
		while($this->matchPatterns(self::$ignoreRegexs))
			continue;

		$token = $this->matchPatterns(self::$tokenRegexs);

		if($token == FALSE)
			throw new ScannerError(
				 "Cannot create token from input string, "
				."remaining string: '$this->inputString'.");
			
		return $token;
	}

	public function getAll() {
		while($token = $this->getNext())
			$this->tokens[] = $token;

		return $this->tokens;
	}

	private function matchPatterns($patterns) {
		$token = false;

		foreach($patterns as $type => $regex) {
			$found = preg_match($regex, $this->inputString, $matches);

			if($found) {
				$token = array("type" => $type, "attribute" => $matches[0]);
				break;
			} 
		}

		if($found) {
			$offset = strlen($matches[0]);
			$this->inputString = substr($this->inputString, $offset);
		}

		return $token;
	}
}
