<?php

// DKA:xsvana01

require_once "automata.php";

//-----
// Vynimka vyvolana ak nie je mozne aplikovat ziadne gramaticke pravidlo
//-----

class ParseError extends Exception {};

//-----
// Skontroluje syntax vstupu a pokusi sa vytvorit konecny automat,
// reprezentovany ako instanciu triedy FiniteAutomata
//-----

class Parser {

	const STATES_SET_PHASE = 0;
	const SYMBOLS_SET_PHASE = 1;
	const RULES_SET_PHASE = 2;
	const START_STATE_PHASE = 3;
	const FINAL_STATES_PHASE = 4;

	private $scanner;
	private $token;
	private $phase;
	private $finiteAutomata;
	private $caseInsensitive;

	public function __construct($scanner, $caseInsensitive=FALSE) {
		$this->scanner = $scanner;
		$this->caseInsensitive = $caseInsensitive;
	}

	public function createFiniteAutomata() {
		$this->token = $this->scanner->getNext();
		$this->finiteAutomata = new FiniteAutomata();
		$this->useFiniteAutomataRule();

		return $this->finiteAutomata;
	}

	private function useFiniteAutomataRule() {
		$this->expectToken("PARENTHESIS_LEFT", "(");

		$this->phase = self::STATES_SET_PHASE;
		$this->token = $this->scanner->getNext();
		$this->useStatesRule();

		$this->phase = self::SYMBOLS_SET_PHASE;
		$this->expectToken("COMMA", ",");
		$this->token = $this->scanner->getNext();
		$this->useSymbolsRule();

		$this->phase = self::RULES_SET_PHASE;
		$this->expectToken("COMMA", ",");
		$this->token = $this->scanner->getNext();
		$this->useRulesRule();

		$this->phase = self::START_STATE_PHASE;
		$this->expectToken("COMMA", ",");
		$this->token = $this->scanner->getNext();
		$this->useStateRule();

		$this->phase = self::FINAL_STATES_PHASE;
		$this->expectToken("COMMA", ",");
		$this->token = $this->scanner->getNext();
		$this->useStatesRule();

		$this->expectToken("PARENTHESIS_RIGHT", ")");
		$this->token = $this->scanner->getNext();

		$this->expectToken("EOF", "EOF");
	}

	private function expectToken($type, $char) {
		$given = $this->token["type"];

		if($given != $type)
			throw new ParseError("Expected '$char' token, $given given");
	}

	//-----
	// Parse states
	//-----

	private function useStatesRule() {
		 $this->expectToken("BRACKET_LEFT", "{");
		 $this->token = $this->scanner->getNext();

		 $this->useStateListRule();

		 $this->expectToken("BRACKET_RIGHT", "}");
		 $this->token = $this->scanner->getNext();
	}

	private function useStateListRule() {
		try { $this->expectToken("STATE", "state"); }
		catch(ParseError $e) { return; };

		$this->useStateRule();
		$this->useNextStatesRule();
	}

	private function useNextStatesRule() {
		try { $this->expectToken("COMMA", ","); }
		catch(ParseError $e) { return; };

		$this->token = $this->scanner->getNext();
		$this->useStateRule();
		$this->useNextStatesRule();
	}

	private function useStateRule() {
		$this->expectToken("STATE", "state");
		
		if($this->caseInsensitive)
			$this->token["attribute"] = strtolower($this->token["attribute"]);

		if($this->phase == self::STATES_SET_PHASE)
			$this->finiteAutomata->addState($this->token["attribute"]);

		if($this->phase == self::START_STATE_PHASE)
			$this->finiteAutomata->setStartState($this->token["attribute"]);

		if($this->phase == self::FINAL_STATES_PHASE)
			$this->finiteAutomata->addFinalState($this->token["attribute"]);

		$this->token = $this->scanner->getNext();
	}

	//-----
	// Parse symbols
	//-----

	private function useSymbolsRule() {
		 $this->expectToken("BRACKET_LEFT", "{");
		 $this->token = $this->scanner->getNext();

		 $this->useSymbolListRule();

		 $this->expectToken("BRACKET_RIGHT", "}");
		 $this->token = $this->scanner->getNext();
	}

	private function useSymbolListRule() {
		try { $this->expectToken("SYMBOL", "symbol"); }
		catch(ParseError $e) { return; };

		$this->useSymbolRule();
		$this->useNextSymbolsRule();
	}

	private function useNextSymbolsRule() {
		try { $this->expectToken("COMMA", ","); }
		catch(ParseError $e) { return; };

		$this->token = $this->scanner->getNext();
		$this->useSymbolRule();
		$this->useNextSymbolsRule();
	}

	private function useSymbolRule() {
		$this->expectToken("SYMBOL", "symbol");

		$tokenAttr = $this->token["attribute"];
		$symbol = substr($tokenAttr, 1, strlen($tokenAttr) - 2);

		if($this->caseInsensitive)
			$symbol = strtolower($symbol);

		$this->finiteAutomata->addSymbol($symbol);

		$this->token = $this->scanner->getNext();
	}

	//-----
	// Parse rules
	//-----

	private function useRulesRule() {
		 $this->expectToken("BRACKET_LEFT", "{");
		 $this->token = $this->scanner->getNext();

		 $this->useRuleListRule();

		 $this->expectToken("BRACKET_RIGHT", "}");
		 $this->token = $this->scanner->getNext();
	}

	private function useRuleListRule() {
		try { $this->expectToken("STATE", "state"); }
		catch(ParseError $e) { return; };

		$this->useRuleRule();
		$this->useNextRulesRule();
	}

	private function useNextRulesRule() {
		try { $this->expectToken("COMMA", ","); }
		catch(ParseError $e) { return; };

		$this->token = $this->scanner->getNext();
		$this->useRuleRule();
		$this->useNextRulesRule();
	}

	private function useRuleRule() {
		$this->expectToken("STATE", "state");
		$state = $this->token["attribute"];

		if($this->caseInsensitive)
			$state = strtolower($state);

		$this->token = $this->scanner->getNext();

		$this->expectToken("SYMBOL", "symbol");
		$tokenAttr = $this->token["attribute"];
		$symbol = substr($tokenAttr, 1, strlen($tokenAttr) - 2);

		if($this->caseInsensitive)
			$symbol = strtolower($symbol);

		$this->token = $this->scanner->getNext();

		$this->expectToken("RULE", "->");
		$this->token = $this->scanner->getNext();

		$this->expectToken("STATE", "state");
		$newState = $this->token["attribute"];

		if($this->caseInsensitive)
			$newState = strtolower($newState);

		$this->token = $this->scanner->getNext();

		$this->finiteAutomata->addRule($state, $symbol, $newState);
	}
}
