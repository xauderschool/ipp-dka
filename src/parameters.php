<?php

// DKA:xsvana01

//-----
// Vynimka vyvolana pri nespravnej kombinacii parametrov na vstupe
//-----

class ParametersError extends Exception {};

//-----
// Spracovanie parametrov zadanych pri spusteni skriptu
//-----

class Parameters {

	//-----
	// Definicia parametrov pre funkciu getopt
	//-----

	private static $shortOpts = "edi";

	private static $longOpts = array(
		"no-epsilon-rules", 
		"determinization", 
		"case-insensitive",
		"help", 
		"input::",
		"output::");

	//-----
	// Zistene nastavenia skriptu a ich defaultne hodnoty
	//-----

	private $input = "";
	private $output = "";
	private $help = FALSE;
	private $removeEpsilonRules = FALSE;
	private $determinize = FALSE;
	private $caseInsensitive = FALSE;

	//-----
	// Zisti zadane parametre a overi ich spravnost
	//-----

	public function getAndCheck() {
		$this->getOptions();
		$this->checkOptions();
	}

	private function getOptions() {
		$options = getopt(self::$shortOpts, self::$longOpts);

		if(array_key_exists("help", $options))
			$this->help = TRUE;

		if(array_key_exists("output", $options) && $options["output"])
			$this->output = $options["output"];

		if(array_key_exists("input", $options) && $options["input"])
			$this->input = $options["input"];

		if(array_key_exists("e", $options) || array_key_exists("no-epsilon-rules", $options))
			$this->removeEpsilonRules = TRUE;

		if(array_key_exists("d", $options) || array_key_exists("determinization", $options))
			$this->determinize = TRUE;

		if(array_key_exists("i", $options) || array_key_exists("case-insensitive", $options))
			$this->caseInsensitive = TRUE;
	}

	public function checkOptions() {
		$cannotHaveHelp = $this->input 
			|| $this->output 
			|| $this->removeEpsilonRules
			|| $this->determinize
			|| $this->caseInsensitive;

		if($this->help && $cannotHaveHelp)
			throw new ParametersError("--help cannot be combined with other parameters");
		
		if($this->determinize && $this->removeEpsilonRules) 
			throw new ParametersError("--determinization and --no-epsilon-rules cannot be combined");
	}

	//-----
	// Gettery na zistenie jednotlivych parametrov skriptu
	//-----

	public function getInput() { return $this->input; }
	public function getOutput() { return $this->output; }
	public function getHelp() { return $this->help; }
	public function getRemoveEpsilonRules() { return $this->removeEpsilonRules; }
	public function getDeterminize() { return $this->determinize; }
	public function getCaseInsensitive() { return $this->caseInsensitive; }
}
