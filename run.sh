#!/usr/bin/env bash

# Testovaci skript pre projekt Determinizacia konecneho automatu
# v jazyku PHP pre predmen IPP

LOG_PATH="./tests/logs/"
IN_PATH="./tests/in/"
SCRIPT="./src/dka.php"


# Mini definicia automatu error na stderr
php ${SCRIPT} --input=${IN_PATH}test01.in --output=${LOG_PATH}test01.out --determinization
echo -n $? > ${LOG_PATH}test01.ret

# Jednoducha determinizacia
php ${SCRIPT} --input=${IN_PATH}test02.in --output=${LOG_PATH}test02.out -d
echo -n $? > ${LOG_PATH}test02.ret

# Pokrocila determinizacia
php ${SCRIPT} --input=${IN_PATH}test03.in --output=${LOG_PATH}test03.out -d
echo -n $? > ${LOG_PATH}test03.ret

# Diakritika a UTF-8
php ${SCRIPT} --input=${IN_PATH}test04.in --output=${LOG_PATH}test04.out
echo -n $? > ${LOG_PATH}test04.ret

# Zlozitejsie formatovanie vstupu
php ${SCRIPT} --input=${IN_PATH}test05.in --output=${LOG_PATH}test05.out
echo -n $? > ${LOG_PATH}test05.ret

# Vstup zo zadania
php ${SCRIPT} --input=${IN_PATH}test06.in --output=${LOG_PATH}test06.out -d
echo -n $? > ${LOG_PATH}test06.ret

# Odstranenie epsilon prechodov
php ${SCRIPT} --input=${IN_PATH}test07.in --output=${LOG_PATH}test07.out -e
echo -n $? > ${LOG_PATH}test07.ret

# Chybna kombinacia parametrov
php ${SCRIPT} --input=${IN_PATH}test08.in --output=${LOG_PATH}test08.out -d --no-epsilon-rules
echo -n $? > ${LOG_PATH}test08.ret

# Chybny format vstupu
php ${SCRIPT} --input=${IN_PATH}test09.in --output=${LOG_PATH}test09.out
echo -n $? > ${LOG_PATH}test09.ret

# Semanticka chyba
php ${SCRIPT} --input=${IN_PATH}test10.in --output=${LOG_PATH}test10.out
echo -n $? > ${LOG_PATH}test10.ret

